Language: Julia
Version: 0.5.1
Compiler: Julia Built-in
Interpreter: Julia Built-in
Test Platform: Windows (64-bit)
Download page: http://julia-lang.org/downloads/

parareal.jl: accepts any fine and coarse integrator (e.g., it can accept any
Runge-Kutta integration technique).
